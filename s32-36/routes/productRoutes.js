const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req,res) =>{

	const userData = auth.decode(req.headers.authorization);
	// If the user is Admin, proceed with the course creation
	if(userData.isAdmin){
		productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
	// If the user is not Admin, send a response "You don't have permission on this page!"
	else{
		res.send("You don't have permission on this page!");
	}
	
})

// Other solution
// router.post("/", auth.verify, (req, res) => {
	
// 	const userData = auth.decode(req.headers.authorization);
// //Using try catch to catch the error due to updates with the node js/express
//try{
	// //Sending the req.body and userData.isAdmin as an argument for the addCourse
	// 	courseController.addCourse(req.body, userData.isAdmin).then(resultFromController => res.send(resultFromController));
//}
//catch(error){
	//res.status(500).json(err)
//}

// });

// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) =>{
	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all active courses
router.get("/", (req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific course
router.get("/:productId", (req,res)=>{
	console.log(req.params.productId);

	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
router.put("/:productId", auth.verify, (req, res)=>{
									// search key		//update
	productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})

// Route to Archive a course
// router.patch("/:courseId/archive", auth.verify, (req, res) =>{
// 	courseControllers.archiveCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
// })

// Route to Archive a course
router.patch("/:productId/archive", auth.verify, (req, res) =>{
	productControllers.archiveProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;