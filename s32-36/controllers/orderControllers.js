const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// NON-ADMIN USER CHECKOUT (Create Order) 
	module.exports.addOrder = async (data) => {
		
		// If non-admin creates an order
		if (data.isAdmin !== true) {
			
			let newOrder = await new Order({
				userId: data.reqBody.userId,
				productId: data.reqBody.productId,
				quantity: data.reqBody.quantity,
				amount: data.reqBody.amount
			});
			
			let total = data.reqBody.quantity * data.reqBody.amount

			// Saves the created object to our database
			
			return newOrder
				.save()
				.then((order, err) => {
					
					// Product creation failed	
					if (err) {
						return err;
					}else{
						return `Product added ${order} total amount is ${total}`;
						// return order;
					}
			});
		
		// User is an admin
		}else{
			return `Access denied`;
		}
	};



// RETRIEVE AUTHENTICATED USER'S ORDERS
	module.exports.listOrders = (userId) => {
	
		return Order
			.find({userId : userId})
			.then(result => {

				// No existing orders
				if(result.length === 0){
					return `No existing orders`;
				
				// Listing current orders
				}else{
					return `Your current orders ${result}`;
				};
		});
	};



// RETRIEVE ALL ORDERS (Admin Only)
	module.exports.allOrders = (isAdmin) => {

		if(isAdmin === true) {
			return Order
				.find({})
				.then(result => {
					return `Here's a list of all orders ${result}`;
				});
		
		// User is not an admin
		}else{
			return `You must be an Admin to perform this action`;
		};
	};
