import {Container} from "react-bootstrap";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "",
		content: "Buy 1 take all",
		destination: "/products",
		label: "Ask me how"
	}

	return(
		<>
			<Container className="loginBG">
			<Banner data={data}/>
        	<Highlights />
        	</Container>
		</>
	)
}